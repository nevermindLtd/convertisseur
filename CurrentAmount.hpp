/*
 * CurrentAmount.hpp
 */
#include <iostream>
#include <libsx.h>
 
struct CurrentAmount{ 
	CurrentAmount();
	CurrentAmount(double valeur, double tauxDeChange, Widget zoneAffichage);
		
	double valeur;
	double tauxDeChange;
	Widget zoneAffichage;
};
