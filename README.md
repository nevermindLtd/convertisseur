# Jérémy MAISSE #

Application graphique de conversion de devise (euro, dollard), qui utilise la librairie libsx.

### Récupérer le code ###

* Cloner ce répertoire à partir de son [URL](https://bitbucket.org/nevermindLtd/convertisseur) dans le répertoire de votre choix sur votre ordinateur :
	
```
cd $HOME/Documents
git clone https://nevermindLtd@bitbucket.org/nevermindLtd/convertisseur.git
```
* Vous devez avoir la libraire libsx. Pour instaler cette libraire: ```sudo apt-get install libsx-dev libsx0```

* Compiler le projet: ```make```

* Exécuter le programme: ```make run```

* Nétoyer le projet:
    * effacer les .o: ```make clean```
    * effacer les .o et executable: ```make cleaner```