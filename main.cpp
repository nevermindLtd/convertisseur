/*
 * main.cpp
 */
#include <iostream>
#include <string>
#include <libsx.h>
#include "Callback.hpp"
#include "CurrentAmount.hpp"

void init_display (int argc, char** argv, void* currentAmount) {
	Widget stringEntry1 = MakeStringEntry(NULL, 148, NULL, NULL);
	Widget button1 = MakeButton("Quit", quit, NULL);
	Widget button2 = MakeButton("Euros", NULL, (void*) currentAmount);
	Widget button3 = MakeButton("Dollars", dollars, (void*) currentAmount);
	
	SetWidgetPos(button1, 10, stringEntry1, PLACE_UNDER, stringEntry1);
	SetWidgetPos(button2, PLACE_RIGHT, button1, PLACE_UNDER, stringEntry1);
	SetWidgetPos(button3, PLACE_RIGHT, button2, PLACE_UNDER, stringEntry1);
	
	// pour passer la zone d'affichage
	CurrentAmount* pCurrentAmount = static_cast<CurrentAmount*>(currentAmount);
	pCurrentAmount->zoneAffichage = stringEntry1;

	// pour gérer les couleurs
	GetStandardColors();
	// pour afficher l’interface
	ShowDisplay();
}

int main(int argc, char** argv) 
{
	CurrentAmount currentAmount;
	
	if (OpenDisplay(argc, argv) == 0) {
		std::cerr << "Erreur: Impossible de lancer le graphique" << std::endl;
	return 1;
	}
	
	init_display(argc, argv, &currentAmount);
	MainLoop();
	return 0;
}


