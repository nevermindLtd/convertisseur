/*
 * Callback.cpp
 */
#include "Callback.hpp"
#include "CurrentAmount.hpp"
#include <cstring>

/*
 * Callback bouton quit
 * Rôle: terminer l’application
 */
void quit(Widget button, void* d) {
	exit(0);
}


/*
 * Callback bouton dollars
 * Rôle: convertit la valeur courante en dollars
 */
void dollars(Widget button, void* d) {
	char* recupTexte = 0;
	
	CurrentAmount* pCurrentAmount = static_cast<CurrentAmount*>(d);
	recupTexte = GetStringEntry(pCurrentAmount->zoneAffichage);
	pCurrentAmount->valeur = std::stod(std::string(recupTexte));

	// calcul
	double resultat = pCurrentAmount->valeur * pCurrentAmount->tauxDeChange;
	// conversion en texte
	std::string texte = std::to_string(resultat);
	char resultText[texte.length()+1];
	std::strcpy(resultText, texte.c_str());
	
	// affichage
	 SetStringEntry(pCurrentAmount->zoneAffichage, resultText);
}
